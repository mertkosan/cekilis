﻿namespace YilbasiCekilisi
{
    partial class Cekilis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.startButton = new System.Windows.Forms.Button();
            this.receiverName = new System.Windows.Forms.TextBox();
            this.receiverMail = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.addBtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.participants = new System.Windows.Forms.ListView();
            this.done = new System.Windows.Forms.Label();
            this.removeBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // startButton
            // 
            this.startButton.Location = new System.Drawing.Point(315, 383);
            this.startButton.Name = "startButton";
            this.startButton.Size = new System.Drawing.Size(112, 36);
            this.startButton.TabIndex = 0;
            this.startButton.Text = "Rastgele";
            this.startButton.UseVisualStyleBackColor = false;
            this.startButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // receiverName
            // 
            this.receiverName.Location = new System.Drawing.Point(227, 22);
            this.receiverName.Name = "receiverName";
            this.receiverName.Size = new System.Drawing.Size(285, 22);
            this.receiverName.TabIndex = 2;
            // 
            // receiverMail
            // 
            this.receiverMail.Location = new System.Drawing.Point(227, 50);
            this.receiverMail.Name = "receiverMail";
            this.receiverMail.Size = new System.Drawing.Size(285, 22);
            this.receiverMail.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(113, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Katılımcı Adı:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(113, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 17);
            this.label2.TabIndex = 6;
            this.label2.Text = "Katılımcı Email:";
            // 
            // addBtn
            // 
            this.addBtn.Location = new System.Drawing.Point(518, 22);
            this.addBtn.Name = "addBtn";
            this.addBtn.Size = new System.Drawing.Size(62, 50);
            this.addBtn.TabIndex = 7;
            this.addBtn.Text = "Ekle";
            this.addBtn.UseVisualStyleBackColor = false;
            this.addBtn.Click += new System.EventHandler(this.addBtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(56, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "Katılımcılar";
            // 
            // participants
            // 
            this.participants.Location = new System.Drawing.Point(59, 104);
            this.participants.Name = "participants";
            this.participants.Size = new System.Drawing.Size(639, 273);
            this.participants.TabIndex = 10;
            this.participants.UseCompatibleStateImageBehavior = false;
            // 
            // done
            // 
            this.done.AutoSize = true;
            this.done.Location = new System.Drawing.Point(450, 393);
            this.done.Name = "done";
            this.done.Size = new System.Drawing.Size(0, 17);
            this.done.TabIndex = 11;
            // 
            // removeBtn
            // 
            this.removeBtn.Location = new System.Drawing.Point(586, 22);
            this.removeBtn.Name = "removeBtn";
            this.removeBtn.Size = new System.Drawing.Size(62, 50);
            this.removeBtn.TabIndex = 12;
            this.removeBtn.Text = "Çıkar";
            this.removeBtn.UseVisualStyleBackColor = false;
            this.removeBtn.Click += new System.EventHandler(this.removeBtn_Click);
            // 
            // Cekilis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(738, 431);
            this.Controls.Add(this.removeBtn);
            this.Controls.Add(this.done);
            this.Controls.Add(this.participants);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.addBtn);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.receiverMail);
            this.Controls.Add(this.receiverName);
            this.Controls.Add(this.startButton);
            this.Name = "Cekilis";
            this.Text = "Çekiliş";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button startButton;
        private System.Windows.Forms.TextBox receiverName;
        private System.Windows.Forms.TextBox receiverMail;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button addBtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView participants;
        private System.Windows.Forms.Label done;
        private System.Windows.Forms.Button removeBtn;
    }
}

