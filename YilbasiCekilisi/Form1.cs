﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace YilbasiCekilisi
{
    public partial class Cekilis : Form
    {
        Dictionary<string, Participant> pool = new Dictionary<string, Participant>();
        bool invalidMail = false;

        public Cekilis()
        {
            InitializeComponent();

            /*PictureBox pb1 = new PictureBox();
            pb1.Image = Image.FromFile(@"images\yilbasi_hediyeleri.jpg");
            pb1.Location = new Point(50, 22);
            this.Controls.Add(pb1);*/

            FormBorderStyle = FormBorderStyle.FixedSingle;

            // Add columns
            participants.Columns.Add("Isim", 200, HorizontalAlignment.Left);
            participants.Columns.Add("E-Mail", -2, HorizontalAlignment.Left);
            participants.View = View.Details;
            participants.ColumnWidthChanging += new ColumnWidthChangingEventHandler(listView_ColumnWidthChanging);
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            startButton.Enabled = false;
            Participant[] parties = new Participant[pool.Count];
            pool.Values.CopyTo(parties, 0);

            Random rnd = new Random();
            Participant[] randomParties = parties.OrderBy(x => rnd.Next()).ToArray();

            /*Console.WriteLine("-----------");
            for (int i = 0; i < parties.Length; i++)
            {
                Console.Write(parties[i].ToString() + " - ");
                Console.WriteLine(randomParties[i].ToString());
            }*/
            for (int i = 0; i < randomParties.Length; i++)
            {
                string receiverMail_ = randomParties[i].email;
                string message = "Hediye alacağın kişi : " + randomParties[(i + 1) % randomParties.Length].name;

                // Command line argument must the the SMTP host.
                SmtpClient client = new SmtpClient();
                client.Port = 587;
                client.Host = "smtp.gmail.com";
                client.EnableSsl = true;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential("umbrella.suite@gmail.com", "Scarlett12");

                MailMessage mm = new MailMessage("donotreply@domain.com", receiverMail_, "Yılbaşı Çekilişi", message);
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);

            }
            done.Text = "Kura Çekildi!";
            startButton.Enabled = true;
        }

        private void addBtn_Click(object sender, EventArgs e)
        {
            string name = receiverName.Text;
            string mail = receiverMail.Text;

            if(name.Equals("") || mail.Equals(""))
            {
                MessageBox.Show("Bütün alanları doldur...", "HATA!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }else if (!IsValidEmail(mail))
            {
                MessageBox.Show("Hatalı mail formatı...", "HATA!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                if (pool.ContainsKey(mail))
                {
                    MessageBox.Show("Bir mail'i iki kez kullanamazsınız...", "HATA!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }else
                {
                    Participant p = new Participant(mail, name);
                    pool.Add(mail, p);

                    //add participant to list
                    string[] arr = new string[2] { name, mail };
                    ListViewItem itm = new ListViewItem(arr);
                    participants.Items.Add(itm);
                }
            }

            receiverName.Clear();
            receiverMail.Clear();
        }

        private void removeBtn_Click(object sender, EventArgs e)
        {
            string mail = receiverMail.Text;
            if (mail.Equals(""))
            {
                MessageBox.Show("Mail alanını doldur...", "HATA!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (!IsValidEmail(mail))
            {
                MessageBox.Show("Hatalı mail formatı...", "HATA!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }else if (!pool.ContainsKey(mail))
            {
                MessageBox.Show("Böyle bir katılımcı yok...", "HATA!", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                pool.Remove(mail);
                foreach (ListViewItem item in participants.Items)
                {
                    if (item.SubItems[1].Text.Equals(mail))
                    {
                        participants.Items.Remove(item);
                    }
                }
            }   
        }

        private void listView_ColumnWidthChanging(object sender, ColumnWidthChangingEventArgs e)
        {
            e.Cancel = true;
            e.NewWidth = participants.Columns[e.ColumnIndex].Width;
        }

        private bool IsValidEmail(string strIn)
        {
            invalidMail = false;
            if (String.IsNullOrEmpty(strIn))
                return false;

            // Use IdnMapping class to convert Unicode domain names.
            try
            {
                strIn = Regex.Replace(strIn, @"(@)(.+)$", this.DomainMapper,
                                      RegexOptions.None, TimeSpan.FromMilliseconds(200));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }

            if (invalidMail)
                return false;

            // Return true if strIn is in valid e-mail format.
            try
            {
                return Regex.IsMatch(strIn,
                      @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                      @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-\w]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                      RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }

        private string DomainMapper(Match match)
        {
            // IdnMapping class with default property values.
            IdnMapping idn = new IdnMapping();

            string domainName = match.Groups[2].Value;
            try
            {
                domainName = idn.GetAscii(domainName);
            }
            catch (ArgumentException)
            {
                invalidMail = true;
            }
            return match.Groups[1].Value + domainName;
        }
    }

    public class Participant
    {
        public string email { get; set; }
        public string name { get; set; }
        
        public Participant(string email, string name)
        {
            this.email = email;
            this.name = name;
        }

        public override string ToString()
        {
            return name + ":" + email;
        }
    }
}
